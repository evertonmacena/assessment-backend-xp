<?php


namespace Framework;


abstract class CrudController
{

    abstract protected function getModel(): string;

    public function index($c, $request)
    {
        $data =  $c[$this->getModel()]->all();
        return $c['blade']->render($this->getModel().'-index', compact('data'));
    }

    public function show($c, $request)
    {
        $id = $request->attributes->get(1);
        $data = $c[$this->getModel()]->get(['id' => $id]);

        return $c['blade']->render($this->getModel().'-show', compact('data'));
    }

    public function create($c, $request)
    {
        $relationship = $c[$this->getModel()]->getRelationship();
        $data = [];
        if ($relationship) {
            foreach ($relationship as $model){
                $data[$model] = $c[$model]->all();
            }
        }

        return $c['blade']->render($this->getModel().'-create', compact('data'));
    }

    public function store($c, $request)
    {
        $c[$this->getModel()]->create($request->request->all());

        return  header("Location: /".$this->getModel());
    }

    public function edit($c, $request)
    {
        $id = $request->attributes->get(1);
        $data = $c[$this->getModel()]->get(['id' => $id]);
        $relationship = $c[$this->getModel()]->getRelationship();

        if ($relationship) {
            foreach ($relationship as $model){
                $data[$model] = $c[$model]->all();
            }
        }

        return $c['blade']->render($this->getModel().'-edit', compact('data'));
    }

    public function update($c, $request)
    {
        $id = $request->attributes->get(1);
        $c[$this->getModel()]->update(['id' => $id], $request->request->all());

        return header("Location: /".$this->getModel());
    }

    public function delete($c, $request)
    {
        $id = $request->attributes->get(1);
        $c[$this->getModel()]->delete(['id' => $id]);

        return header("Location: /".$this->getModel());
    }

}