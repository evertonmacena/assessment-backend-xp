#Instruções para executar
 ## Docker-compose 
 1. Instalar as depencias:
 ```
 composer install
 ```
 2. Rodar os containers: 
 ````
 docker-composer up -d
 ````
 
 ## Localmente
 1. Instalar as depencias:
 ````
     composer install
 ````
2. Alterar configurações de banco de dados em `config/containers.php` ou criar um banco no seguinte padrão:

| userusername 	| password 	| database 	|
|--------------	|----------	|----------	|
| teste        	| teste    	| teste    	|