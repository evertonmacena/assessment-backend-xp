@extends('layout')
@section('content')
<h1 class="title new-item">Edit Product {{$data['id']}}</h1>

<form action="/product" method="post">
    <div class="input-field">
        <label for="id" class="label">Product SKU</label>
        <input type="number" id="id"  name="id" class="input-text" value="{{$data['id']}}"/>
    </div>
    <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" name="name" class="input-text" value="{{$data['name']}}" />
    </div>
    <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="number" id="price" name="price" class="input-text" value="{{$data['price']}}" />
    </div>
    <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="number" id="quantity" name="quantity" class="input-text" value="{{$data['quantity']}}"/>
    </div>
    <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple id="category"  class="input-text">
            @foreach($data['categories'] as $category)
                <option value={{$category['id']}}>{{$category['name']}}</option>
            @endforeach
        </select>
    </div>
    <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" name="description" class="input-text">value="{{$data['description']}}"</textarea>
    </div>
    <div class="actions-form">
        <a href="/products" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Product" />
    </div>
</form>
@endsection