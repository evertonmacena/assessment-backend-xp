<?php

use Jenssegers\Blade\Blade;

$router->add("GET", "/", function () use($container){

    return $container['blade']->render('dashboard');
});

$router->add("GET", "/products", "App\controllers\ProductController::index");

$router->add("GET", "/product/(\d+)", "App\controllers\ProductController::show");

$router->add("GET", "/product", "App\controllers\ProductController::create");

$router->add("GET", "/product/(\d+)", "App\controllers\ProductController::edit");

$router->add("POST", "/product", "App\controllers\ProductController::store");

$router->add("POST", "/product/(\d+)/update", "App\controllers\ProductController::update");

$router->add("GET", "/product/(\d+)/delete", "App\controllers\ProductController::delete");

$router->add("GET", "/categories", "App\controllers\CategoryController::index");

$router->add("GET", "/category/(\d+)", "App\controllers\CategoryController::show");

$router->add("GET", "/category", "App\controllers\CategoryController::create");

$router->add("GET", "/category/(\d+)", "App\controllers\CategoryController::edit");

$router->add("POST", "/category", "App\controllers\CategoryController::store");

$router->add("POST", "/category/(\d+)/update", "App\controllers\CategoryController::update");

$router->add("GET", "/category/(\d+)/delete", "App\controllers\CategoryController::delete");


