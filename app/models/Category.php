<?php


namespace App\models;


use Framework\Model;

class Category extends Model
{
    protected $table = "categories";

    protected $relationship = ["products"];

}