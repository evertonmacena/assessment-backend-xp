<?php


namespace App\models;


use Framework\Model;

class Product extends Model
{
    protected $table = "products";

    protected $relationship = ['categories', 'products'];

}