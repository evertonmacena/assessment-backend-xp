@extends('layout')
@section('content')
<h1 class="title new-item">Category SKU {{$data['id']}}</h1>

<form action="/category" method="post">
    <div class="input-field">
        <label for="id" class="label">Category SKU</label>
        <input type="number" id="id"  name="id" class="input-text" value="{{$data['id']}}"/>
    </div>
    <div class="input-field">
        <label for="name" class="label">Category  Name</label>
        <input type="text" id="name" name="name" class="input-text" value="{{$data['name']}}" />
    </div>
    <div class="actions-form">
        <a href="/categories" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Category" />
    </div>
</form>
@endsection