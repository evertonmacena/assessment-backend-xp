<?php


namespace Framework;


use Pimple\Container;

abstract class Model
{
    protected $db;
    protected $queryBuilder;
    protected $table;
    protected $relationship = [];

    public function __construct(Container $container)
    {
        $this->db = $container['db'];
        $this->queryBuilder = new QueryBuilder;

        if (!$this->table) {
            $table = explode('\\', \get_called_class());
            $table = array_pop($table);
            $this->table = strtolower($table);
        }
    }

    public function get(array $conditions)
    {
        $query = $this->queryBuilder->select($this->table)
            ->where($conditions)
            ->getData();

        $stmt = $this->db->prepare($query->sql);
        $stmt->execute($query->bind);

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }

    public function all(array $conditions = [])
    {
        $query = $this->queryBuilder->select($this->table)
            ->where($conditions)
            ->getData();

        $stmt = $this->db->prepare($query->sql);
        $stmt->execute($query->bind);

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function create(array $data)
    {
        $data = $this->setData($data);

        $query = $this->queryBuilder->insert($this->table, $data)
            ->getData();

        $stmt = $this->db->prepare($query->sql);
        $stmt->execute($query->bind);

        $result = $this->get(['id'=>$this->db->lastInsertId()]);

        return $result;
    }

    public function update(array $conditions, array $data)
    {
        $data = $this->setData($data);

        $query = $this->queryBuilder->update($this->table, $data)
            ->where($conditions)
            ->getData();

        $stmt = $this->db->prepare($query->sql);
        $stmt->execute(array_values($query->bind));

        $result = $this->get($conditions);


        return $result;
    }

    public function delete(array $conditions)
    {
        $result = $this->get($conditions);

        $query = $this->queryBuilder->delete($this->table)
            ->where($conditions)
            ->getData();

        $stmt = $this->db->prepare($query->sql);
        $stmt->execute($query->bind);

        return $result;
    }

    protected function setData($data)
    {
        foreach ($data as $field => $value) {
            $method = str_replace('_', '', $field);
            $method = ucwords($method);
            $method = str_replace(' ', '', $method);
            $method = "set{$method}";
            if (method_exists($this, $method)) {
                $data[$field] = $this->$method($value);
            }
        }

        return $data;
    }

    public function getRelationship():array
    {
        return $this->relationship;
    }

/*
 * apply dynamic relationship
 */

//    public function attach($relationship, array $data)
//    {
//        if (in_array($relationship, $this->relationship)){
//            $data = $this->setData($data);
//
//            if ($this->checkTableExists($relationship.'_'.$this->table)) {
//                $table_relationship = $relationship.'_'.$this->table;
//            } elseif ($this->checkTableExists($this->table.'_'.$relationship)){
//                $table_relationship = $this->table.'_'.$relationship;
//            } else {
//                return false;
//            }
//
//            $query = $this->queryBuilder->insert($table_relationship, $data)
//                ->getData();
//
//            $stmt = $this->db->prepare($query->sql);
//            $stmt->execute($query->bind);
//
//            $result = $this->get(['id'=>$this->db->lastInsertId()]);
//
//            return $result;
//        }
//    }
//
//    public function checkTableExists($table)
//    {
//        $test = "SELECT 1 FROM " . $table . " LIMIT 1";
//        $test = $this->db->query($test); //$db needs to be PDO instance
//
//        if($test) {
//            return true;
//        }
//
//        return false;
//
//    }

}